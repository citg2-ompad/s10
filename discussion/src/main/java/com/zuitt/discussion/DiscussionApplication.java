package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Retrieving all posts
	// http://localhost:8080/post
//	@RequestMapping(value = "/post",method= RequestMethod.GET)
	@GetMapping("/posts")
	public String getPost(){
		return  "All post retrieved";
	}
	// Creating a new post
	// http://localhost:8080/posts
//	@RequestMapping(value="/posts",method = RequestMethod.POST)
	@PostMapping("/posts")
	public String createPost(){
		return "New post created";
	}
	//Retrieving a single post

	//	@RequestMapping(value="/posts/{postid}",method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post "+postid;
	}
	//Deleting a post
//	@RequestMapping(value="posts/delete/{postid}", method = RequestMethod.DELETE)
	@DeleteMapping("posts/delete/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post "+postid+" has been deleted";
	}
	//Updating a post
	//@RequestBody annotation allows us to retrieve information sent via the request generated by the frontend.
//	@RequestMapping(value= "/posts/{postid}",method = RequestMethod.PUT)
	@PutMapping("/posts/{postid}")
	@ResponseBody
	//@ResponseBody annotation allows the response to be sent back to the client in JSON format
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}
	//Retrieving posts of a particular user
//	@RequestMapping(value="/myPosts", method = RequestMethod.GET)
	@GetMapping("/myPosts")
	//@RequestHeader annotation allows us to grab information from the request header
	public String getMyposts(@RequestHeader(value = "Authorization") String user){
		if(user.isEmpty()){
			user = "Unauthorized Access";
		}
		else {
			return "Posts for " + user + " have been retrieved.";
		}
		return user;
	}
	//The "@GetMapping, @PostMapping, @DeleteMapping, and @PutMapping" annotations are called


	//ACTIVITY S10

	//a. GET all users
	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	//b. Create User
	@PostMapping("/users")
	public String postUser(){
		return "New user created";
	}
	//c. Get a specific User
	@GetMapping("/users/{user}")
	public String getUser(@PathVariable String user){
		return "name: "+user;
	}
	//d. Delete user
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable String userid,@RequestHeader(value="authorization") String user){
		if(user.isEmpty()){
			user = "Unauthorized access";
		}
		else {
			user = "The user " + userid + " has been deleted";
		}
		return user;
	}

	//e. Update user
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User putUser(@PathVariable String userid,@RequestBody User user){
		return user;
	}
}

